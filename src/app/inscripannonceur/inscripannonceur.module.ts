import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { InscripannonceurPageRoutingModule } from './inscripannonceur-routing.module';

import { InscripannonceurPage } from './inscripannonceur.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    InscripannonceurPageRoutingModule
  ],
  declarations: [InscripannonceurPage]
})
export class InscripannonceurPageModule {}
