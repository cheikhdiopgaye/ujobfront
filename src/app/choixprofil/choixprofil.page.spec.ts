import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ChoixprofilPage } from './choixprofil.page';

describe('ChoixprofilPage', () => {
  let component: ChoixprofilPage;
  let fixture: ComponentFixture<ChoixprofilPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChoixprofilPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ChoixprofilPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
