import { TestBed } from '@angular/core/testing';

import { InscripcandidatService } from './inscripcandidat.service';

describe('InscripcandidatService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: InscripcandidatService = TestBed.get(InscripcandidatService);
    expect(service).toBeTruthy();
  });
});
