import { TestBed } from '@angular/core/testing';

import { ChoixprofilService } from './choixprofil.service';

describe('ChoixprofilService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ChoixprofilService = TestBed.get(ChoixprofilService);
    expect(service).toBeTruthy();
  });
});
