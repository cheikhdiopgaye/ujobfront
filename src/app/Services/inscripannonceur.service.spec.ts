import { TestBed } from '@angular/core/testing';

import { InscripannonceurService } from './inscripannonceur.service';

describe('InscripannonceurService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: InscripannonceurService = TestBed.get(InscripannonceurService);
    expect(service).toBeTruthy();
  });
});
