import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { InscricandidatPageRoutingModule } from './inscricandidat-routing.module';

import { InscricandidatPage } from './inscricandidat.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    InscricandidatPageRoutingModule
  ],
  declarations: [InscricandidatPage]
})
export class InscricandidatPageModule {}
