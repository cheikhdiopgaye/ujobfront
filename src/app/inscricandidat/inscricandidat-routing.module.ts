import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { InscricandidatPage } from './inscricandidat.page';

const routes: Routes = [
  {
    path: '',
    component: InscricandidatPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class InscricandidatPageRoutingModule {}
