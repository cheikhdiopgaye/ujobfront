import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)},
  {
    path: 'connection',
    loadChildren: () => import('./connection/connection.module').then( m => m.ConnectionPageModule)
  },
  {
    path: 'choixprofil',
    loadChildren: () => import('./choixprofil/choixprofil.module').then( m => m.ChoixprofilPageModule)
  },
  {
    path: 'inscripannonceur',
    loadChildren: () => import('./inscripannonceur/inscripannonceur.module').then( m => m.InscripannonceurPageModule)
  },
  {
    path: 'inscricandidat',
    loadChildren: () => import('./inscricandidat/inscricandidat.module').then( m => m.InscricandidatPageModule)
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
