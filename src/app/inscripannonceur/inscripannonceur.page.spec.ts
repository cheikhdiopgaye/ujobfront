import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { InscripannonceurPage } from './inscripannonceur.page';

describe('InscripannonceurPage', () => {
  let component: InscripannonceurPage;
  let fixture: ComponentFixture<InscripannonceurPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InscripannonceurPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(InscripannonceurPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
